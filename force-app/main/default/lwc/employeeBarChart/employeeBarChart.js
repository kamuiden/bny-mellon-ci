import { LightningElement } from 'lwc';
import { NavigationMixin  } from 'lightning/navigation';

export default class EmployeeBarChart extends NavigationMixin(LightningElement) {
    
    goToListView(){
        this[NavigationMixin.Navigate]({
            type: "standard__objectPage",
            attributes: {
                objectApiName: "EmployeeHistory__c",
                actionName: "list"
            },
            state: {
                filterName: '00B5Y00000B1JfrUAF'
            }
        });
    }
}