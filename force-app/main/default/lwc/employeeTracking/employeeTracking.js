import { LightningElement } from 'lwc';

export default class EmployeeTracking extends LightningElement {
    filterTypes = [];

  renderedCallback() {
    if (this.filterTypes.length === 0) {
      this.filterTypes = [
        ...new Set(
          [...this.template.querySelectorAll('c-chart')].map(
            (chart) => chart.type
          )
        )
      ];
    }
  }

  displayAllCharts() {
    this.template.querySelectorAll('c-chart').forEach((item) => {
      item.closest('c-sample-app-item').classList.remove('slds-hide');
    });
  }

  clickBar(event){
    var element = this.getElementAtEvent(event);
    console.log(element[0]._model.label);
    
    var activePoints = this.getElementsAtEventForMode(event, 'point', this.options);
    var firstPoint = activePoints[0];
    var label = this.data.labels[firstPoint._index];
    var value = this.data.datasets[firstPoint._datasetIndex].data[firstPoint._index];
    alert(label + ": " + value);
    
  }

  displayFilteredCharts(event) {
    this.template.querySelectorAll('c-chart').forEach((item) => {
      const parent = item.closest('c-employee-bar-chart');
      if (!parent) return;
      if (item.type.toUpperCase() === event.target.innerText) {
        parent.classList.remove('slds-hide');
      } else {
        parent.classList.add('slds-hide');
      }
    });
  }

}